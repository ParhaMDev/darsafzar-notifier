import 'package:darsafzarnotifier/consts.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:darsafzarnotifier/screens/moreDetailScreen.dart';
import 'package:provider/provider.dart';

import '../ThemeProvider.dart';

class AnnouncementsReusableCard extends StatelessWidget {
  final String title, content, author;

  AnnouncementsReusableCard({this.title, this.content, this.author});

  @override
  Widget build(BuildContext context) {
    final theme = Provider.of<ThemeProvider>(context);
    return FlatButton(
      padding: EdgeInsets.all(0),
      onPressed: () {
        showModalBottomSheet(
            context: context,
            builder: (context) => MoreDetailScreen(
                content: content, title: title, author: author));
      },
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 10),
        child: Container(
          width: double.infinity,
          height: 160,
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 13),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  title.trim(),
                  maxLines: 2,
                  style: kTitleOfAnnouncementsReusableCardTextStyle,
                  textDirection: TextDirection.rtl,
                ),
                Padding(
                  padding: EdgeInsets.only(top: 18.0),
                  child: Text(
                    content.trim(),
                    maxLines: 3,
                    style: kContentOfAnnouncementsTextStyle,
                    overflow: TextOverflow.ellipsis,
                    textDirection: TextDirection.rtl,
                  ),
                ),
              ],
            ),
          ),
          decoration: BoxDecoration(
              color:  theme.isDarkTheme == false ? Colors.blueAccent : Color(0xFF383c61),
              borderRadius: BorderRadius.circular(20)),
        ),
      ),
    );
  }
}
