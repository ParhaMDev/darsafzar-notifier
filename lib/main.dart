import 'package:darsafzarnotifier/ThemeData.dart';
import 'package:darsafzarnotifier/ThemeProvider.dart';
import 'package:darsafzarnotifier/screens/SplashScreen.dart';
import 'package:darsafzarnotifier/screens/searchScreen.dart';
import 'package:flutter/material.dart';
import 'package:darsafzarnotifier/screens/welcomeScreen.dart';
import 'package:darsafzarnotifier/screens/AnnouncementsScreen.dart';
import 'package:provider/provider.dart';
import 'screens/loginScreen.dart';
import 'screens/registerScreen.dart';
import 'screens/coursesScreen.dart';
import 'screens/AnnouncementsScreen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (BuildContext context) => ThemeProvider(),
      child: Consumer<ThemeProvider>(
        builder: (BuildContext context, theme, Widget child) {
          theme.setTheme();
          return MaterialApp(
            theme: themeData(context),
            darkTheme: darkThemeData(context),
            themeMode:
                theme.isDarkTheme == false ? ThemeMode.light : ThemeMode.dark,
            debugShowCheckedModeBanner: false,
            home: Splash(),
            routes: {
              WelcomeScreen.id: (context) => WelcomeScreen(),
              RegisterScreen.id: (context) => RegisterScreen(),
              LoginScreen.id: (context) => LoginScreen(),
              CoursesScreen.id: (context) => CoursesScreen(),
              SearchScreen.id: (context) => SearchScreen(),
              AnnouncementsScreen.id: (context) => AnnouncementsScreen(),
            },
          );
        },
      ),
    );
  }
}
