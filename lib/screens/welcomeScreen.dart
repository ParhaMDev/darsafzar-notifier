import 'package:darsafzarnotifier/screens/loginScreen.dart';
import 'package:darsafzarnotifier/screens/registerScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:darsafzarnotifier/consts.dart';
import 'package:provider/provider.dart';
import 'package:darsafzarnotifier/ThemeProvider.dart';

class WelcomeScreen extends StatelessWidget {
  static const String id = 'welcomeScreen';

  @override
  Widget build(BuildContext context) {
    final theme = Provider.of<ThemeProvider>(context);
    return SafeArea(
      child: Scaffold(
        backgroundColor:
            theme.isDarkTheme == false ? Colors.white : Color(0XFF222222),
        body: Padding(
          padding: EdgeInsets.all(15.0),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'DarsAfzer Notifier',
                  style: TextStyle(
                    letterSpacing: 1.1,
                    color: theme.isDarkTheme == false
                        ? Colors.black54
                        : Colors.grey,
                    fontFamily: 'MuseoModerno',
                    fontSize: 33,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 12.0),
                  child: MaterialButton(
                    minWidth: double.infinity,
                    padding: EdgeInsets.symmetric(vertical: 14),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(40.0),
                    ),
                    onPressed: () {
                      Navigator.pushNamed(context, LoginScreen.id);
                    },
                    color: theme.isDarkTheme == false
                        ? Colors.lightBlueAccent
                        : Colors.lightBlueAccent.shade200,
                    child: Text(
                      'ورود',
                      style: kLoginRegisterForgotButtonTextStyle,
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16.0),
                  child: MaterialButton(
                    minWidth: double.infinity,
                    padding: EdgeInsets.symmetric(vertical: 14),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(40.0),
                    ),
                    onPressed: () {
                      Navigator.pushNamed(context, RegisterScreen.id);
                    },
                    color: theme.isDarkTheme == false
                        ? Colors.blueAccent
                        : Colors.blueAccent.shade200,
                    child: Text(
                      'ثبت نام',
                      style: kLoginRegisterForgotButtonTextStyle,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
