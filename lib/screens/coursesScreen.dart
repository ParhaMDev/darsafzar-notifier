import 'package:cool_alert/cool_alert.dart';
import 'package:darsafzarnotifier/consts.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:darsafzarnotifier/Config.dart';
import 'package:darsafzarnotifier/screens/searchScreen.dart';
import 'package:darsafzarnotifier/screens/welcomeScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import '../ThemeProvider.dart';
import 'coursesReusableCard.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class CoursesScreen extends StatefulWidget {
  static const String id = 'coursesScreen';

  @override
  _CoursesScreenState createState() => _CoursesScreenState();
}

class _CoursesScreenState extends State<CoursesScreen> {
  bool showIndicator = false;

  @override
  void initState() {
    super.initState();
    this.getCourses();
  }

  List data;

  Future<String> getCourses() async {
    setState(() {
      showIndicator = true;
    });
    var header = {
      'Authorization': Config.token,
      'Content-Type': 'application/json',
    };
    http.Response response =
        await http.get('${Config.url}/user/courses', headers: header);
    if (response.statusCode == 200) {
      setState(() {
        var jsonToData = jsonDecode(response.body);
        data = jsonToData['courses'];
        if (data != null) {
          Config.courseId.clear();
          for (int i = 0; i < data.length; i++) {
            Config.courseId.add(data[i]["id"]);
          }
        }
      });
    }
    setState(() {
      showIndicator = false;
    });
    return "success";
  }

  Widget showWidget() {
    if (data == null) {
      return Center(
          child: Text(
        showIndicator == false
            ? 'درسی وجود ندارد. برای افزودن روی + کلیک کنید.'
            : '',
        textDirection: TextDirection.rtl,
        style: kNoCourseOrAnnouncementTextStyle,
      ));
    } else {
      return ListView.builder(
        itemCount: data == null ? 0 : data.length,
        itemBuilder: (BuildContext context, int index) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              CoursesReusableCard(
                courseName: data[index]['title'],
                teacherName: data[index]['professor'],
                id: data[index]['id'],
                index: index,
              ),
            ],
          );
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    final theme = Provider.of<ThemeProvider>(context);
    return ModalProgressHUD(
      inAsyncCall: showIndicator,
      color: Color(0xFF0D0C0E),
      progressIndicator: CircularProgressIndicator(
        color: Colors.blueAccent,
      ),
      child: Scaffold(
        backgroundColor:
            theme.isDarkTheme == false ? Colors.blueAccent : Color(0xFF0D0C0E),
        floatingActionButton: FloatingActionButton(
          backgroundColor: theme.isDarkTheme ? kSecondaryDarkColor : kAccentLightColor,
          elevation: theme.isDarkTheme == false ? 15 : 0,
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => SearchScreen()));
          },
          child: Icon(
            Icons.add,
            color: theme.isDarkTheme ? Colors.grey : Colors.blueAccent,
          ),
        ),
        body: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.symmetric(vertical: 5.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: EdgeInsets.fromLTRB(18, 0, 18, 0),
                      child: Center(
                        child: Text(
                          'DarsAfzar Notifier',
                          style: kCoursesScreenLogo,
                        ),
                      ),
                    ),
                    Column(
                      children: [
                        IconButton(
                            padding: EdgeInsets.all(0),
                            onPressed: () async {
                              CoolAlert.show(
                                  context: context,
                                  type: CoolAlertType.confirm,
                                  title: 'آیا مطمئنید؟',
                                  confirmBtnText: '  بله',
                                  cancelBtnText: 'خیر',
                                  onCancelBtnTap: () {
                                    Navigator.pop(context);
                                  },
                                  onConfirmBtnTap: () async {
                                    SharedPreferences sharedPreferences =
                                        await SharedPreferences.getInstance();
                                    await sharedPreferences.clear();
                                    Config.token = null;
                                    Navigator.pushNamedAndRemoveUntil(context,
                                        WelcomeScreen.id, (route) => false);
                                  });
                            },
                            icon: Icon(
                              Icons.power_settings_new,
                              color: Colors.white,
                              size: 20,
                            )),
                        IconButton(
                            padding: EdgeInsets.all(0),
                            onPressed: () {
                              theme.changeTheme();
                            },
                            icon: theme.isDarkTheme == false
                                ? SvgPicture.asset('icons/Sun.svg',
                                    width: 16, height: 16, color: Colors.white)
                                : SvgPicture.asset('icons/Moon.svg',
                                    width: 16,
                                    height: 16,
                                    color: Colors.pinkAccent.shade100)),
                      ],
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.only(right: 10, left: 10, top: 15),
                  decoration: BoxDecoration(
                      color: theme.isDarkTheme == false
                          ? Colors.white
                          : Color(0xFF202023),
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(30),
                          topLeft: Radius.circular(30))),
                  child: showWidget(),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
