import 'dart:ui';
import 'package:darsafzarnotifier/ThemeProvider.dart';
import 'package:darsafzarnotifier/consts.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MoreDetailScreen extends StatelessWidget {
  String title, content, author;

  MoreDetailScreen({this.title, this.content, this.author});

  @override
  Widget build(BuildContext context) {
    final theme = Provider.of<ThemeProvider>(context);
    return Container(
      color: theme.isDarkTheme == false ? Color(0xff757575) : Color(0xFF0c0c0c),
      child: BackdropFilter(
        filter: ImageFilter.blur(sigmaY: 4.2, sigmaX: 4.2),
        child: Consumer<ThemeProvider>(
            builder: (BuildContext context, theme, Widget child) {
          return Container(
            decoration: BoxDecoration(
                color: theme.isDarkTheme == false
                    ? Colors.white
                    : Color(0xFF202023),
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(30),
                    topLeft: Radius.circular(30))),
            child: ListView(
              shrinkWrap: true,
              children: [
                Padding(
                  padding: EdgeInsets.only(top: 20.0, left: 25, right: 25),
                  child: Text(
                    title.trim(),
                    style: theme.isDarkTheme
                        ? kDarkTitleOfAnnouncementTextStyle
                        : kLightTitleOfAnnouncementTextStyle,
                    textDirection: TextDirection.rtl,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 20.0, left: 25, right: 25),
                  child: Text(
                    'نویسنده: ${author}',
                    style: theme.isDarkTheme
                        ? kDarkAuthorAndContentTextStyle
                        : kLightAuthorAndContentTextStyle,
                    textDirection: TextDirection.rtl,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      top: 20, left: 25.0, right: 25, bottom: 45),
                  child: Text(
                    content.trim(),
                    style: theme.isDarkTheme
                        ? kDarkAuthorAndContentTextStyle
                        : kLightAuthorAndContentTextStyle,
                    textDirection: TextDirection.rtl,
                  ),
                )
              ],
            ),
          );
        }),
      ),
    );
  }
}
