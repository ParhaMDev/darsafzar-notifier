import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

const kTitleTextLightColor = Color(0xFF101112);
const kTitleTextDarkColor = Colors.white;
const kAccentIconLightColor = Colors.blueAccent;
const kAccentIconDarkColor = Colors.white;
const kSecondaryLightColor = Color(0xFFE4E9F2);
const kSecondaryDarkColor = Color(0xFF212020);
const kBackgroundDarkColor = Color(0xFF3A3A3A);
const kAccentLightColor = Colors.white;
const kAccentDarkColor = Color(0xFF4E4E4E);
const kSurfaceDarkColor = Color(0xFF222225);
// Icon Colors
const kPrimaryIconLightColor = Color(0xFFECEFF5);
const kPrimaryIconDarkColor = Color(0xFF232323);
// Text Colors
const kBodyTextColorLight = Colors.blueAccent;
const kBodyTextColorDark = Color(0xFF7C7C7C);

TextStyle kLoginRegisterForgotButtonTextStyle = TextStyle(
  color: Colors.white,
  fontFamily: 'iransans',
  fontWeight: FontWeight.bold,
  fontSize: 16.0,
);

TextStyle kHintTextStyle = TextStyle(fontSize: 14 , color: Colors.grey);

TextStyle kConfirmButtonTextStyle =
    TextStyle(fontSize: 15.5, color: Colors.white, fontFamily: 'iransans');

TextStyle kNoCourseOrAnnouncementTextStyle = TextStyle(
  color: Colors.black38,
  fontSize: 15,
);

TextStyle kCoursesScreenLogo = TextStyle(
    letterSpacing: 1.1,
    fontSize: 29.0,
    fontFamily: 'MuseoModerno',
    color: Colors.white);
TextStyle kAnnouncementsScreenLogo = TextStyle(
    letterSpacing: 1.1,
    fontFamily: 'MuseoModerno',
    fontSize: 35.0,
    color: Colors.white);

TextStyle kCourseAndProfessorNameTextStyle = TextStyle(
  color: Colors.white,
  fontFamily: 'Vazir',
  fontSize: 13.2,
);
TextStyle kTitleOfAnnouncementsReusableCardTextStyle = TextStyle(
    color: Colors.white,
    fontSize: 15,
    fontFamily: 'Vazir',
    fontWeight: FontWeight.w600);
TextStyle kContentOfAnnouncementsTextStyle =
    TextStyle(color: Colors.white, fontSize: 14, fontFamily: 'Vazir');
TextStyle kSearchItemTitleTextStyle =
    TextStyle(color: Colors.white, fontFamily: 'Vazir', fontSize: 12.0);
TextStyle kRegisteredButtonTextStyle = TextStyle(
  color: Colors.white,
  fontFamily: 'Vazir',
  fontSize: 12.0,
);
TextStyle kAddCourseButtonTextStyle =
    TextStyle(color: Colors.blueAccent, fontSize: 12.0, fontFamily: 'Vazir');
TextStyle kDarkTitleOfAnnouncementTextStyle = TextStyle(
    height: 2,
    fontSize: 18,
    fontWeight: FontWeight.bold,
    color: Colors.grey,
    fontFamily: 'iransans');
TextStyle kLightTitleOfAnnouncementTextStyle = TextStyle(
    height: 2,
    fontSize: 18,
    fontWeight: FontWeight.bold,
    color: Colors.blueAccent,
    fontFamily: 'iransans');

TextStyle kDarkAuthorAndContentTextStyle = TextStyle(
    height: 2, fontSize: 15, color: Colors.grey, fontFamily: 'iransans');
TextStyle kLightAuthorAndContentTextStyle = TextStyle(
    height: 2, fontSize: 15, color: Colors.blueAccent, fontFamily: 'iransans');

InputDecoration buildInputDecoration(
    String errorText, String hintText, bool validateOfField) {
  return InputDecoration(
    errorText: validateOfField ? errorText : null,
    hintText: hintText,
    hintStyle: kHintTextStyle,
    contentPadding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
    border: OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(32.0)),
    ),
    enabledBorder: OutlineInputBorder(
      borderSide: BorderSide(color: Colors.lightBlueAccent, width: 1.0),
      borderRadius: BorderRadius.all(Radius.circular(32.0)),
    ),
    focusedBorder: OutlineInputBorder(
      borderSide: BorderSide(color: Colors.lightBlueAccent, width: 2.0),
      borderRadius: BorderRadius.all(Radius.circular(32.0)),
    ),
  );
}
