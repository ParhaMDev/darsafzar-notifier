import 'package:flutter/material.dart';
import 'consts.dart';

ThemeData themeData(BuildContext context) {
  return ThemeData(
    fontFamily: 'iransans',
    accentColor: kAccentLightColor,
    scaffoldBackgroundColor: Colors.blueAccent,
    colorScheme: ColorScheme.light(
      onSecondary: Colors.blueAccent,
      secondary: kSecondaryLightColor,
    ),
    backgroundColor: Colors.white,
    iconTheme: IconThemeData(color: kBodyTextColorLight),
    accentIconTheme: IconThemeData(color: kAccentIconLightColor),
    primaryIconTheme: IconThemeData(color: kPrimaryIconLightColor),
    textTheme: TextTheme().copyWith(
        bodyText1: TextStyle(
            color: Colors.blueAccent, fontSize: 15, fontFamily: 'iransans'),
        bodyText2: TextStyle(
            color: kBodyTextColorLight,
            height: 2,
            fontSize: 15,
            fontFamily: 'vazir'),
        headline4: TextStyle(
            color: kBodyTextColorLight,
            fontFamily: 'vazir',
            height: 2,
            fontSize: 17,
            fontWeight: FontWeight.bold),
        headline3: TextStyle(
            letterSpacing: 1.1,
            fontSize: 29.0,
            fontFamily: 'MuseoModerno',
            color: Colors.white)),
  );
}

// Dark Them
ThemeData darkThemeData(BuildContext context) {
  return ThemeData.dark().copyWith(
    accentColor: kAccentDarkColor,
    scaffoldBackgroundColor: Color(0xFF0D0C0E),
    colorScheme: ColorScheme.dark(
        secondary: kSecondaryDarkColor, onSecondary: Colors.white70),
    backgroundColor: kBackgroundDarkColor,
    iconTheme: IconThemeData(color: kBodyTextColorDark),
    accentIconTheme: IconThemeData(color: kAccentIconDarkColor),
    primaryIconTheme: IconThemeData(color: kPrimaryIconDarkColor),
    textTheme: TextTheme().copyWith(
        bodyText1:
            TextStyle(color: Colors.grey, fontSize: 15, fontFamily: 'iransans'),
        bodyText2: TextStyle(
            color: Colors.white, height: 2, fontSize: 15, fontFamily: 'vazir'),
        headline4: TextStyle(
            fontFamily: 'vazir',
            color: Colors.white,
            height: 2,
            fontSize: 17,
            fontWeight: FontWeight.bold)),
  );
}
