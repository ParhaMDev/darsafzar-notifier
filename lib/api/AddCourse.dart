import 'package:darsafzarnotifier/Config.dart';
import 'package:http/http.dart' as http;

class AddCourse {
  Future<void> addCourse(int id) async {
    var header = {
      'Authorization': Config.token,
      'Content-Type': 'application/json',
    };
    final response = await http.post('${Config.url}/user/course',
        headers: header, body: '''{\r\n    "courseId": "$id"\r\n}''');
    if(response.statusCode == 201) {
      Config.statusOfAddCourse = 201;
      Config.courseId.add(id);
    }else {
      Config.statusOfAddCourse = 402;
      throw Exception('Failed to add Course');
    }
  }
}
