import 'dart:convert';
import 'package:darsafzarnotifier/consts.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import '../Config.dart';
import '../ThemeProvider.dart';
import 'AnnouncementsReusableCard.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

class AnnouncementsScreen extends StatefulWidget {
  static const String id = 'AnnouncementsScreen';
  int courseId;

  AnnouncementsScreen({this.courseId});

  @override
  _AnnouncementsScreenState createState() => _AnnouncementsScreenState();
}

class _AnnouncementsScreenState extends State<AnnouncementsScreen> {
  List data;
  bool showIndicator = false;

  @override
  void initState() {
    super.initState();
    setState(() {
      this.getAnnouncement(widget.courseId);
    });
  }

  Future<String> getAnnouncement(int id) async {
    setState(() {
      showIndicator = true;
    });
    var response = await http.get('${Config.url}/course/$id/entries');
    if (response.statusCode == 200) {
      setState(() {
        var jsonToData = jsonDecode(response.body);
        data = jsonToData['entries'];
        showIndicator = false;
      });
    } else {
      setState(() {
        showIndicator = false;
      });
      throw Exception('something went wrong');
    }
    return 'success';
  }

  Widget noCourseWidget() {
    if (data == null) {
      return Center(
        child: Text(showIndicator == false ? 'اطلاعیه ای وجود ندارد' : '',
            textDirection: TextDirection.rtl,
            style: kNoCourseOrAnnouncementTextStyle),
      );
    } else
      return ListView.builder(
          itemCount: data == null ? 0 : data.length,
          itemBuilder: (BuildContext context, int index) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                AnnouncementsReusableCard(
                  title: data[index]['title'],
                  content: data[index]['content'],
                  author: data[index]['author'],
                ),
              ],
            );
          });
  }

  @override
  Widget build(BuildContext context) {
    final theme = Provider.of<ThemeProvider>(context);
    return ModalProgressHUD(
      inAsyncCall: showIndicator,
      color: Color(0xFF0D0C0E),
      progressIndicator: CircularProgressIndicator(
        color: Colors.blueAccent,
      ),
      child: SafeArea(
        child: Scaffold(
          backgroundColor:
          theme.isDarkTheme== false ? Colors.blueAccent : Color(0xFF0D0C0E),
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(
                  top: 15,
                  bottom: 15,
                ),
                child: Center(
                  child: Text('DarsAfzar Notifier',
                      style: kAnnouncementsScreenLogo),
                ),
              ),
              Expanded(
                  child: Container(
                padding: EdgeInsets.only(right: 10, left: 10, top: 15),
                decoration: BoxDecoration(
                    color:
                    theme.isDarkTheme == false ? Colors.white : Color(0xFF202023),
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(30),
                        topLeft: Radius.circular(30))),
                child: noCourseWidget(),
              ))
            ],
          ),
        ),
      ),
    );
  }
}
