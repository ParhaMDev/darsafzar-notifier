import 'package:cool_alert/cool_alert.dart';
import 'package:darsafzarnotifier/Models/forget_model.dart';
import 'package:darsafzarnotifier/api/forgetPasswordRequest.dart';
import 'package:darsafzarnotifier/consts.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';

import '../ThemeProvider.dart';

class ForgetPasswordScreen extends StatefulWidget {
  @override
  _ForgetPasswordScreenState createState() => _ForgetPasswordScreenState();
}

class _ForgetPasswordScreenState extends State<ForgetPasswordScreen> {
  bool _validateOfEmail = false;
  final _textOfEmail = TextEditingController();
  bool showSpinner = false;
  String email;
  String hintText = 'پست الکترونیک';
  String errorText = 'فیلد نمیتواند خالی باشد';
  ForgetModel forgetModel;

  void checkFields() {
    setState(() {
      _textOfEmail.text.isEmpty
          ? _validateOfEmail = true
          : _validateOfEmail = false;
    });
  }

  @override
  void initState() {
    super.initState();
    forgetModel = new ForgetModel();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Provider.of<ThemeProvider>(context);
    return ModalProgressHUD(
      inAsyncCall: showSpinner,
      child: Scaffold(
        backgroundColor:
            theme.isDarkTheme == false ? Colors.white : Color(0XFF222222),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: EdgeInsets.symmetric(vertical: 7.0, horizontal: 15),
              child: TextFormField(
                  controller: _textOfEmail,
                  style: TextStyle(
                      fontSize: 16,
                      fontFamily: 'iransans',
                      color: theme.isDarkTheme ? Colors.white : Colors.black),
                  textAlign: TextAlign.center,
                  keyboardType: TextInputType.emailAddress,
                  onChanged: (value) => forgetModel.email = value,
                  decoration: buildInputDecoration(
                      errorText, hintText, _validateOfEmail)),
            ),
            SizedBox(height: 20),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 7.0, horizontal: 15.0),
              child: MaterialButton(
                onPressed: () async {
                  checkFields();
                  if (_validateOfEmail == false) {
                    setState(() {
                      showSpinner = true;
                    });
                    ForgetPassword forget = ForgetPassword();
                    await forget.sendRequest(forgetModel);
                    setState(() {
                      showSpinner = false;
                    });
                    CoolAlert.show(
                        context: context,
                        type: CoolAlertType.info,
                        text:
                            'ایمیل بازیابی برای شما ارسال شد. برای تغییر رمز ایمیلتان را چک کنید',
                        title: '',
                        confirmBtnText: '  تایید',
                        confirmBtnTextStyle: kConfirmButtonTextStyle);
                  }
                },
                child: Text(
                  'بازیابی',
                  style: kLoginRegisterForgotButtonTextStyle,
                ),
                color: Colors.blueAccent,
                minWidth: double.infinity,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(40.0),
                ),
                padding: EdgeInsets.symmetric(vertical: 14),
              ),
            )
          ],
        ),
      ),
    );
  }
}
