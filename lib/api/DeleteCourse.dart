import 'package:darsafzarnotifier/Config.dart';
import 'package:http/http.dart' as http;

class DeleteCourse {
  Future delete(int id) async {
    var header = {
      'Authorization': Config.token,
      'Content-Type': 'application/json',
    };
    var request = http.Request(
        'DELETE', Uri.parse('${Config.url}/user/course'));
    request.body = '''{\r\n    "courseId": "$id"\r\n}''';
    request.headers.addAll(header);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      print('course deleted');
      for (int i = 0; i < Config.courseId.length; i++) {
        if (Config.courseId[i] == id) {
          Config.courseId.removeAt(i);
          break;
        }
      }
    } else {
      throw Exception('somthing went wrong');
    }
  }
}
