import 'package:darsafzarnotifier/Models/login_model.dart';
import 'package:http/http.dart' as http;
import 'package:darsafzarnotifier/Config.dart';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

class LoginService {
  Future<LoginResponseModel> login(LoginRequestModel requestModel) async {
    var body = jsonEncode(requestModel.toJson());
    final response =
        await http.post("${Config.url}/user/login", body: body);
    if (response.statusCode == 200) {
      Config.statusOfLogin = 200;
      var data = jsonDecode(response.body);
      var token = data['token'];
      Config.token = token;
      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();
      await sharedPreferences.setString('token', Config.token);
    } else if ((response.statusCode == 400) ||
        (response.statusCode == 402) ||
        (response.statusCode == 403) ||
        (response.statusCode == 409) ||
        (response.statusCode == 422 ||
            (response.statusCode == 504) ||
            (response.statusCode == 502))) {
      Config.statusOfLogin = 403;
    }
  }
}
