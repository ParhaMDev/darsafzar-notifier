import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ThemeProvider extends ChangeNotifier {
  bool _isDarkTheme = true;

  void setTheme() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getBool('isLightTheme') == null) {
      sharedPreferences.setBool('isLightTheme', _isDarkTheme);
    }else{
      getTheme();
    }
  }

  void changeTheme() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    _isDarkTheme = sharedPreferences.getBool('isLightTheme');
    print(_isDarkTheme);
    _isDarkTheme = !_isDarkTheme;
    print(_isDarkTheme);
    sharedPreferences.setBool('isLightTheme', _isDarkTheme);
    notifyListeners();
  }

  Future<bool> getTheme() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    _isDarkTheme = sharedPreferences.getBool('isLightTheme');
  }

  bool get isDarkTheme {
    setTheme();
    return _isDarkTheme;
  }
}
