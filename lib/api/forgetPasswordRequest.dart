import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:darsafzarnotifier/Models/forget_model.dart';
import '../Config.dart';

class ForgetPassword {
  String url = "${Config.url}/user/email/recovery";

  Future<void> sendRequest(ForgetModel forgetModel) async {
    var body = jsonEncode(forgetModel.toJsoon());
    final response = await http.post(url, body: body);
    if (response.statusCode == 200) {
      print('email sent');
    } else {
      throw Exception('somthing went wrong');
    }
  }
}
