import 'package:darsafzarnotifier/screens/coursesScreen.dart';
import 'package:flutter/material.dart';
import 'package:splashscreen/splashscreen.dart';
import 'package:darsafzarnotifier/screens/welcomeScreen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:darsafzarnotifier/Config.dart';
import 'package:darsafzarnotifier/api/TokenValidation.dart';

Future<Widget> getInfo() async {
  await getToken();
  if (Config.token != null) {
    TokenValidation tokenValidation = TokenValidation();
    await tokenValidation.validation();
  }
  if (Config.token == null) {
   return new WelcomeScreen();
  } else if (Config.valid == false) {
    return new WelcomeScreen();
  } else if ((Config.token.isNotEmpty) && (Config.valid == true)) {
    return new CoursesScreen();
  }
}

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {


  @override
  Widget build(BuildContext context) {
    return new SplashScreen(
      navigateAfterFuture: getInfo(),
      image: new Image.asset('images/D.png'),
      title:  Text(
        'DarsAfzar Notifier',
        style: TextStyle(
          letterSpacing: 1.1,
          color: Colors.black87,
          fontFamily: 'MuseoModerno',
          fontSize: 33,
          fontWeight: FontWeight.bold,
        ),
      ),
      photoSize: 150.0,
      loadingText: Text("Loading"),
      loaderColor: Colors.blue,
    );
  }
}

Future getToken() async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String token = sharedPreferences.getString('token');
  Config.token = token;
}
