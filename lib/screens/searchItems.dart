import 'dart:ui';
import 'package:darsafzarnotifier/consts.dart';
import 'package:flutter/material.dart';
import 'package:darsafzarnotifier/api/AddCourse.dart';
import 'package:cool_alert/cool_alert.dart';
import 'package:darsafzarnotifier/Config.dart';
import 'package:provider/provider.dart';
import '../ThemeProvider.dart';

class SearchItems extends StatefulWidget {
  var teacherName, courseName, id;

  SearchItems({this.courseName, this.teacherName, this.id});

  @override
  _SearchItemsState createState() => _SearchItemsState();
}

class _SearchItemsState extends State<SearchItems> {
  AddCourse addCourse = AddCourse();
  bool _isButtonPressed = false;
  Color buttonColor = Colors.white.withOpacity(0.8);
  Color buttonTextColor = Colors.blueAccent;

  void buttonPressedCallBack() {
    setState(() {
      _isButtonPressed = true;
      buttonTextColor = Colors.white;
      buttonColor = Colors.blueAccent.shade100;
    });
  }



  @override
  Widget build(BuildContext context) {
    final theme = Provider.of<ThemeProvider>(context);
    Widget changeButton() {
      if ((_isButtonPressed == true) || (Config.checkId(widget.id) == true)) {
        return Text(
          'ثبت شده',
          textAlign: TextAlign.center,
          style: kRegisteredButtonTextStyle,
        );
      } else {
        return Text('افزودن',
            textAlign: TextAlign.center,
            style: TextStyle(
                color: theme.isDarkTheme ? Colors.white : Colors.blueAccent,
                fontSize: 12.0,
                fontFamily: 'Vazir'));
      }
    }
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10.0),
      child: Container(
        width: double.infinity,
        height: 90,
        decoration: BoxDecoration(
          color: theme.isDarkTheme == false
              ? Colors.blueAccent
              : Color(0xFF383c61),
          borderRadius: BorderRadius.all(Radius.circular(20)),
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              MaterialButton(
                minWidth: 75,
                padding: EdgeInsets.all(0),
                color: Config.checkId(widget.id) == false
                    ? theme.isDarkTheme == false
                        ? Colors.white.withOpacity(0.8)
                        : Color(0xFF63699b)
                    : Colors.blueAccent.shade100,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10))),
                onPressed: () async {
                  if ((_isButtonPressed == true) ||
                      (Config.checkId(widget.id) == true)) {
                    buttonPressedCallBack();
                  } else if (_isButtonPressed == false &&
                      Config.checkId(widget.id) == false) {
                    await addCourse.addCourse(widget.id);
                    if (Config.statusOfAddCourse == 201) {
                      CoolAlert.show(
                        context: context,
                        type: CoolAlertType.success,
                        title: ' درس افزوده شد',
                        confirmBtnText: '  تایید',
                        confirmBtnTextStyle: kConfirmButtonTextStyle,
                      );
                      buttonPressedCallBack();
                    }
                  }
                },
                child: changeButton(),
              ),
              Padding(
                padding: EdgeInsets.only(left: 8.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Text(
                        '${widget.courseName}',
                        style: kSearchItemTitleTextStyle,
                        textDirection: TextDirection.rtl,
                      ),
                    ),
                    Text(
                      '${widget.teacherName}',
                      style: kSearchItemTitleTextStyle,
                      textDirection: TextDirection.rtl,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
