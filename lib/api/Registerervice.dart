import 'package:darsafzarnotifier/Models/login_model.dart';
import 'package:http/http.dart' as http;
import 'package:darsafzarnotifier/Config.dart';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

class RegisterService {
  Future<LoginResponseModel> register(LoginRequestModel requestModel) async {
    var body = jsonEncode(requestModel.toJson());
    final response =
    await http.post("${Config.url}/user/register", body: body);
    if (response.statusCode == 201) {
      var data = jsonDecode(response.body);
      var token = data['token'];
      Config.statusOfRegistration = 201;
      Config.token = token;
      SharedPreferences sharedPreferences =
      await SharedPreferences.getInstance();
      await sharedPreferences.setString('token', Config.token);
    } else if (response.statusCode == 409) {
      Config.statusOfRegistration = 409;
    } else if (response.statusCode == 422) {
      Config.statusOfRegistration = 422;
    } else if (response.statusCode == 504 || response.statusCode == 502) {
    Config.statusOfRegistration = 502;
    }
  }
}
