import 'package:darsafzarnotifier/Config.dart';
import 'package:darsafzarnotifier/screens/coursesScreen.dart';
import 'package:flutter/material.dart';
import 'package:darsafzarnotifier/screens/searchItems.dart';
import 'package:flutter/rendering.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';

import '../ThemeProvider.dart';

class SearchScreen extends StatefulWidget {
  static const String id = 'searchScreen';

  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  final nameHolder = TextEditingController();

  clearTextInput() {
    nameHolder.clear();
  }

  bool _showSpinner = false;
  bool isItEnd = false;
  List data, category;
  String courseName;
  String field = '0';
  int page = 0;
  int perPage = 25;
  ScrollController _scrollController = ScrollController();
  List<Color> textColor = [
    Colors.white,
    Colors.blueAccent,
    Colors.blueAccent,
    Colors.blueAccent,
    Colors.blueAccent,
    Colors.blueAccent,
  ];
  List<Color> lightButtonColor = [
    Colors.blueAccent.withOpacity(0.95),
    Colors.blueAccent.withOpacity(0.1),
    Colors.blueAccent.withOpacity(0.1),
    Colors.blueAccent.withOpacity(0.1),
    Colors.blueAccent.withOpacity(0.1),
    Colors.blueAccent.withOpacity(0.1),
  ];

  List<Color> darkButtonColor = [
    Colors.blueAccent.shade100,
    Colors.blueAccent.withOpacity(0.1),
    Colors.blueAccent.withOpacity(0.1),
    Colors.blueAccent.withOpacity(0.1),
    Colors.blueAccent.withOpacity(0.1),
    Colors.blueAccent.withOpacity(0.1),
  ];

  @override
  void initState() {
    super.initState();
    setState(() {
      _showSpinner = true;
    });
    this.getJsonData('');
    _scrollController.addListener(() {
      double maxScroll = _scrollController.position.maxScrollExtent;
      double currentScroll = _scrollController.position.pixels;

      if (maxScroll == currentScroll) {
        if (isItEnd == false) {
          setState(() {
            page++;
          });
          getJsonData('');
        }
      }
    });
  }

  Future<bool> _onWillPop() async {
    return (await Navigator.pushNamedAndRemoveUntil(
                context, CoursesScreen.id, (route) => false)
            // ignore: missing_return
            .then((value) {
          setState(() {});
        })) ??
        false;
  }

  Future<String> getJsonData(String course) async {
    setState(() {
      _showSpinner = true;
    });
    http.Response response = await http
        .get('${Config.url}/course/$course/field/$field/page/$page/$perPage');
    if (response.statusCode == 200) {
      if (category == null) {
        await getCategory();
      }
      setState(() {
        var jsonToData = jsonDecode(response.body);
        List newData = jsonToData['courses'];
        if (data == null) {
          data = jsonToData['courses'];
        } else
          for (int i = 0; i < newData.length; i++) {
            data.add(newData[i]);
          }
        _showSpinner = false;
      });
    } else {
      setState(() {
        _showSpinner = false;
        isItEnd = true;
      });
      throw Exception('something went wrong');
    }
    return "success";
  }

  Future getCategory() async {
    http.Response response = await http.get('${Config.url}/field');
    if (response.statusCode == 200) {
      setState(() {
        var jsonToCategory = jsonDecode(response.body);
        category = (jsonToCategory['fields']);
        category.insert(0, {'id': '0', 'name': 'همه'});
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final theme = Provider.of<ThemeProvider>(context);
    return WillPopScope(
      onWillPop: _onWillPop,
      child: SafeArea(
        child: Scaffold(
          body: ModalProgressHUD(
            color: Color(0xFF0D0C0E),
            progressIndicator: CircularProgressIndicator(
              color: Colors.blueAccent,
            ),
            inAsyncCall: _showSpinner,
            child: Container(
              color:
                  theme.isDarkTheme == false ? Colors.white : Color(0xFF202023),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding:
                        EdgeInsets.symmetric(vertical: 15.0, horizontal: 10),
                    child: Container(
                      height: 50,
                      width: double.infinity,
                      decoration: BoxDecoration(
                          boxShadow: theme.isDarkTheme == false
                              ? kElevationToShadow[6]
                              : kElevationToShadow[0],
                          color: theme.isDarkTheme == false
                              ? Colors.white
                              : Color(0xFF141414),
                          borderRadius: BorderRadius.circular(15)),
                      child: Row(
                        children: [
                          IconButton(
                              icon: Icon(
                                Icons.search,
                                color: theme.isDarkTheme == false
                                    ? Colors.blueAccent
                                    : Colors.blueAccent.shade100,
                              ),
                              onPressed: () async {
                                data.clear();
                                setState(() {
                                  isItEnd = false;
                                  page = 0;
                                  _showSpinner = true;
                                });
                                await getJsonData(courseName);
                                setState(() {
                                  _showSpinner = false;
                                });
                              }),
                          Expanded(
                            child: TextField(
                              textInputAction: TextInputAction.search,
                              onSubmitted: (val) async {
                                setState(() {
                                  isItEnd = false;
                                  data.clear();
                                  page = 0;
                                  _showSpinner = true;
                                });
                                await getJsonData(courseName);
                                setState(() {
                                  _showSpinner = false;
                                });
                              },
                              controller: nameHolder,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: 'نام درس یا استاد',
                                hintStyle: TextStyle(
                                  fontFamily: 'iransans',
                                  fontSize: 14,
                                  color: Colors.grey,
                                ),
                                hintTextDirection: TextDirection.rtl,
                                contentPadding: EdgeInsets.symmetric(
                                    vertical: 10.0, horizontal: 20.0),
                              ),
                              textDirection: TextDirection.rtl,
                              onChanged: (value) {
                                courseName = value;
                              },
                              style: TextStyle(
                                  fontSize: 14.0, fontFamily: 'iransans' , color: theme.isDarkTheme ? Colors.white : Colors.black),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 8.0),
                    child: Container(
                        height: 27,
                        child: ListView.builder(
                          itemCount: category == null ? 0 : category.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Padding(
                              padding: EdgeInsets.symmetric(horizontal: 6.0),
                              child: FlatButton(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10)),
                                  color: theme.isDarkTheme == false
                                      ? lightButtonColor[index]
                                      : darkButtonColor[index],
                                  onPressed: () async {
                                    if (field != category[index]['id']) {
                                      setState(() {
                                        isItEnd = false;
                                        courseName = '';
                                        clearTextInput();
                                        for (int i = 0;
                                            i < lightButtonColor.length;
                                            i++) {
                                          theme.isDarkTheme == false
                                              ? lightButtonColor[i] = Colors
                                                  .blueAccent
                                                  .withOpacity(0.1)
                                              : darkButtonColor[i] = Colors
                                                  .blueAccent
                                                  .withOpacity(0.1);
                                          textColor[i] = Colors.blueAccent;
                                        }
                                        theme.isDarkTheme == false
                                            ? lightButtonColor[index] = Colors
                                                .blueAccent
                                                .withOpacity(0.95)
                                            : darkButtonColor[index] =
                                                Colors.blueAccent.shade100;
                                        textColor[index] = Colors.white;
                                      });
                                      setState(() {
                                        field = category[index]['id'];
                                      });
                                      data.clear();
                                      setState(() {
                                        page = 0;
                                        _showSpinner = true;
                                      });
                                      await getJsonData('');
                                      setState(() {
                                        _showSpinner = false;
                                      });
                                    }
                                  },
                                  child: Text(
                                    category[index]['name'],
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontFamily: 'iransans',
                                        color: textColor[index],
                                        fontSize: 12.5),
                                  )),
                            );
                          },
                          reverse: true,
                          scrollDirection: Axis.horizontal,
                        )),
                  ),
                  Expanded(
                    child: ListView.builder(
                      controller: _scrollController,
                      itemCount: data == null ? 0 : data.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Padding(
                          padding: EdgeInsets.only(right: 10.0, left: 10.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              new SearchItems(
                                teacherName: data[index]['professor'],
                                courseName: data[index]['title'],
                                id: data[index]['id'],
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
