import 'package:darsafzarnotifier/api/LoginService.dart';
import 'package:darsafzarnotifier/consts.dart';
import 'package:darsafzarnotifier/screens/forgetPasswordScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:darsafzarnotifier/screens/coursesScreen.dart';
import 'package:darsafzarnotifier/Models/login_model.dart';
import 'package:darsafzarnotifier/Config.dart';
import 'package:cool_alert/cool_alert.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';

import '../ThemeProvider.dart';

class LoginScreen extends StatefulWidget {
  static const String id = 'loginScreen';

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _textOfEmail = TextEditingController();
  final _textOfPassword = TextEditingController();
  bool _validateOfEmail = false;
  bool _validateOfPassword = false;
  bool _show = false;
  LoginRequestModel requestModel;
  String hintTextOfEmail = 'پست الکترونیک';
  String hintTextOfPassword = 'رمز عبور';
  String errorText = 'فیلد نمیتواند خالی باشد';
  int status;

  void checkFields() {
    setState(() {
      _textOfEmail.text.isEmpty
          ? _validateOfEmail = true
          : _validateOfEmail = false;
      _textOfPassword.text.isEmpty
          ? _validateOfPassword = true
          : _validateOfPassword = false;
    });
  }

  @override
  void initState() {
    super.initState();
    requestModel = new LoginRequestModel();
  }

  final scaffoldKey = GlobalKey<ScaffoldState>();
  GlobalKey<FormState> globalFormKey = new GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final theme = Provider.of<ThemeProvider>(context);
    return ModalProgressHUD(
      inAsyncCall: _show,
      color: Color(0xFF0D0C0E),
      progressIndicator: CircularProgressIndicator(
        color: Colors.blueAccent,
      ),
      child: Scaffold(
        backgroundColor:
            theme.isDarkTheme == false ? Colors.white : Color(0XFF222222),
        key: scaffoldKey,
        body: SafeArea(
            child: Container(
          child: Form(
            key: globalFormKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 7.0, horizontal: 15),
                  child: TextFormField(
                      controller: _textOfEmail,
                      style: TextStyle(
                          fontSize: 16,
                          fontFamily: 'iransans',
                          color:
                              theme.isDarkTheme ? Colors.white : Colors.black),
                      textAlign: TextAlign.center,
                      keyboardType: TextInputType.emailAddress,
                      onChanged: (value) => requestModel.email = value,
                      decoration: buildInputDecoration(
                          errorText, hintTextOfEmail, _validateOfEmail)),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 7.0, horizontal: 15),
                  child: TextFormField(
                      controller: _textOfPassword,
                      style: TextStyle(
                          fontSize: 16,
                          fontFamily: 'iransans',
                          color:
                              theme.isDarkTheme ? Colors.white : Colors.black),
                      textAlign: TextAlign.center,
                      obscureText: true,
                      onChanged: (value) => requestModel.password = value,
                      decoration: buildInputDecoration(
                          errorText, hintTextOfPassword, _validateOfPassword)),
                ),
                SizedBox(
                  height: 20.0,
                ),
                Padding(
                  padding:
                      EdgeInsets.symmetric(vertical: 7.0, horizontal: 15.0),
                  child: MaterialButton(
                    color: theme.isDarkTheme == false
                        ? Colors.lightBlueAccent
                        : Colors.lightBlueAccent.shade200,
                    minWidth: double.infinity,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(40.0),
                    ),
                    padding: EdgeInsets.symmetric(vertical: 16),
                    onPressed: () {
                      setState(() async {
                        checkFields();
                        if ((!_validateOfEmail) && (!_validateOfPassword)) {
                          if (validateAndSave()) {
                            _show = true;
                            LoginService loginService = LoginService();
                            await loginService.login(requestModel);
                            setState(() {
                              status = Config.statusOfLogin;
                              _show = false;
                            });

                            if (status == 200) {
                              CoolAlert.show(
                                  context: context,
                                  type: CoolAlertType.success,
                                  title: '!خوش آمدید',
                                  confirmBtnText: '  تایید',
                                  confirmBtnTextStyle: kConfirmButtonTextStyle,
                                  onConfirmBtnTap: () {
                                    Navigator.of(context).pushAndRemoveUntil(
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                CoursesScreen()),
                                        (route) => false);
                                  });
                            } else if (status == 403) {
                              CoolAlert.show(
                                  context: context,
                                  type: CoolAlertType.error,
                                  title: '!خطا',
                                  text: "ایمیل یا رمز عبور اشتباه است",
                                  confirmBtnText: '  تایید',
                                  confirmBtnTextStyle: kConfirmButtonTextStyle,
                                  onConfirmBtnTap: () {
                                    Navigator.pop(context);
                                  });
                            }
                          }
                        }
                      });
                    },
                    child: Text(
                      'ورود',
                      style: kLoginRegisterForgotButtonTextStyle,
                    ),
                  ),
                ),
                TextButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ForgetPasswordScreen()));
                    },
                    child: Text(
                      'رمز خود را فراموش کردید؟',
                      style: TextStyle(
                          fontSize: 13,
                          fontFamily: 'iransans',
                          color: Colors.blueAccent),
                    ))
              ],
            ),
          ),
        )),
      ),
    );
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }
}
