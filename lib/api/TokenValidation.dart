import 'dart:async';
import 'package:darsafzarnotifier/Config.dart';
import 'package:http/http.dart' as http;

class TokenValidation {

  var url = "${Config.url}/user/token/valid";

  Future<void> validation() async {
    var header = {
      'Authorization':Config.token,
    };
    print(header);
    final response = await http.get(url, headers: header);
    print(response.statusCode);
    if (response.statusCode == 200) {
      Config.valid = true;
    } else
      Config.valid = false;
  }
}
