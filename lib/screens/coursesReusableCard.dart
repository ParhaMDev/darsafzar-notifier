import 'package:cool_alert/cool_alert.dart';
import 'package:darsafzarnotifier/consts.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:darsafzarnotifier/api/DeleteCourse.dart';
import 'package:provider/provider.dart';
import '../ThemeProvider.dart';
import 'AnnouncementsScreen.dart';
import 'coursesScreen.dart';

class CoursesReusableCard extends StatefulWidget {
  final String courseName, teacherName;
  final int id, index;

  CoursesReusableCard(
      {@required this.courseName,
      @required this.teacherName,
      this.id,
      this.index});

  @override
  _CoursesReusableCardState createState() => _CoursesReusableCardState();
}

class _CoursesReusableCardState extends State<CoursesReusableCard> {
  DeleteCourse deleteCourse = DeleteCourse();

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      padding: EdgeInsets.all(0),
      onPressed: () => Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => AnnouncementsScreen(courseId: widget.id))),
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10),
        child: Consumer<ThemeProvider>(
            builder: (BuildContext context, theme, Widget child) {
          return Container(
            decoration: BoxDecoration(
                color: theme.isDarkTheme == false
                    ? Colors.blueAccent
                    : Color(0xFF383c61),
                borderRadius: BorderRadius.circular(20)),
            width: double.infinity,
            height: 92,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                IconButton(
                    color: CupertinoColors.white,
                    icon: Icon(Icons.close, size: 20),
                    onPressed: () async {
                      await deleteCourse.delete(widget.id);
                      CoolAlert.show(
                          context: context,
                          type: CoolAlertType.success,
                          title: 'درس حذف شد',
                          confirmBtnText: '  تایید',
                          confirmBtnTextStyle: kConfirmButtonTextStyle,
                          onConfirmBtnTap: () {
                            Navigator.pushNamedAndRemoveUntil(
                                    context, CoursesScreen.id, (route) => false)
                                .then((value) {
                              setState(() {});
                            });
                          });
                    }),
                Container(
                  padding: EdgeInsets.only(top: 12.3, right: 15),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(right: 3),
                        child: Text(
                          '${widget.courseName}',
                          style: kCourseAndProfessorNameTextStyle,
                          textDirection: TextDirection.rtl,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 24.0, right: 3),
                        child: Text(
                          '${widget.teacherName}',
                          style: kCourseAndProfessorNameTextStyle,
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                          textDirection: TextDirection.rtl,
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          );
        }),
      ),
    );
  }
}
