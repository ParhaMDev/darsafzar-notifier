import 'package:darsafzarnotifier/Config.dart';
import 'package:darsafzarnotifier/Models/login_model.dart';
import 'package:darsafzarnotifier/api/Registerervice.dart';
import 'package:darsafzarnotifier/consts.dart';
import 'package:darsafzarnotifier/screens/coursesScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cool_alert/cool_alert.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';
import '../ThemeProvider.dart';

class RegisterScreen extends StatefulWidget {
  static const String id = 'registerScreen';

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  var confirm, password;
  final _textOfEmail = TextEditingController();
  final _textOfPassword = TextEditingController();
  final _textOfConfirm = TextEditingController();
  String hintTextOfEmail = 'پست الکترونیک';
  String hintTextOfPassword = 'رمز عبور';
  String hintTextOfConfirm = 'تکرار رمز عبور';
  String errorText = 'فیلد نمیتواند خالی باشد';
  bool _validateOfEmail = false;
  bool _validateOfConfirm = false;
  bool _validateOfPassword = false;
  bool _show = false;
  LoginRequestModel requestModel;
  int status;

  void checkFields() {
    setState(() {
      _textOfEmail.text.isEmpty
          ? _validateOfEmail = true
          : _validateOfEmail = false;
      _textOfPassword.text.isEmpty
          ? _validateOfPassword = true
          : _validateOfPassword = false;
      _textOfConfirm.text.isEmpty
          ? _validateOfConfirm = true
          : _validateOfConfirm = false;
    });
  }

  @override
  void initState() {
    super.initState();
    requestModel = new LoginRequestModel();
  }

  final scaffoldKey = GlobalKey<ScaffoldState>();
  GlobalKey<FormState> globalFormKey = new GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final theme = Provider.of<ThemeProvider>(context);
    return ModalProgressHUD(
      inAsyncCall: _show,
      color: Color(0xFF0D0C0E),
      progressIndicator: CircularProgressIndicator(
        color: Colors.blueAccent,
      ),
      child: Scaffold(
        backgroundColor:
            theme.isDarkTheme == false ? Colors.white : Color(0XFF222222),
        key: scaffoldKey,
        body: SafeArea(
            child: Container(
          child: Form(
            key: globalFormKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 7.0, horizontal: 15),
                  child: TextFormField(
                    controller: _textOfEmail,
                    style: TextStyle(
                        fontSize: 16,
                        fontFamily: 'iransans',
                        color: theme.isDarkTheme ? Colors.white : Colors.black),
                    textAlign: TextAlign.center,
                    keyboardType: TextInputType.emailAddress,
                    onChanged: (value) => requestModel.email = value,
                    decoration: buildInputDecoration(
                        errorText, hintTextOfEmail, _validateOfEmail),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 7.0, horizontal: 15),
                  child: TextFormField(
                    controller: _textOfPassword,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 16,
                        fontFamily: 'iransans',
                        color: theme.isDarkTheme ? Colors.white : Colors.black),
                    obscureText: true,
                    onChanged: (value) {
                      requestModel.password = value;
                      password = value;
                    },
                    decoration: buildInputDecoration(
                        errorText, hintTextOfPassword, _validateOfPassword),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 7.0, horizontal: 15),
                  child: TextFormField(
                    controller: _textOfConfirm,
                    style: TextStyle(
                        fontSize: 16,
                        fontFamily: 'iransans',
                        color: theme.isDarkTheme ? Colors.white : Colors.black),
                    textAlign: TextAlign.center,
                    obscureText: true,
                    onChanged: (value) => confirm = value,
                    decoration: buildInputDecoration(
                        errorText, hintTextOfConfirm, _validateOfConfirm),
                  ),
                ),
                SizedBox(
                  height: 20.0,
                ),
                Padding(
                  padding:
                      EdgeInsets.symmetric(vertical: 7.0, horizontal: 15.0),
                  child: MaterialButton(
                    padding: EdgeInsets.symmetric(vertical: 16),
                    minWidth: double.infinity,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(40.0),
                    ),
                    onPressed: () {
                      setState(() async {
                        checkFields();
                        if (confirm != password) {
                          CoolAlert.show(
                            context: context,
                            type: CoolAlertType.error,
                            title: '!خطا',
                            text: "رمز ها یکسان نیستند",
                            confirmBtnText: '  تایید',
                            confirmBtnTextStyle: kConfirmButtonTextStyle,
                          );
                        } else if ((!_validateOfEmail) &&
                            (!_validateOfPassword) &&
                            (!_validateOfConfirm) &&
                            (confirm == password)) {
                          if (validateAndSave()) {
                            _show = true;
                            RegisterService registerService = RegisterService();
                            await registerService.register(requestModel);
                            setState(() {
                              status = Config.statusOfRegistration;
                              _show = false;
                            });
                            if (status == 201) {
                              CoolAlert.show(
                                  context: context,
                                  type: CoolAlertType.success,
                                  title: '!خوش آمدید',
                                  text:
                                      'برای ادامه ایمیل را تایید کنید در غیر این صورت اطلاعیه ها برای شما ارسال نخواهد شد',
                                  confirmBtnText: '  تایید',
                                  confirmBtnTextStyle: kConfirmButtonTextStyle,
                                  onConfirmBtnTap: () {
                                    Navigator.pushNamedAndRemoveUntil(context,
                                        CoursesScreen.id, (route) => false);
                                  });
                            } else if (status == 409) {
                              CoolAlert.show(
                                  context: context,
                                  type: CoolAlertType.error,
                                  title: '!خطا',
                                  text: "این ایمیل قبلا ثبت شده",
                                  confirmBtnText: '  تایید',
                                  confirmBtnTextStyle: kConfirmButtonTextStyle,
                                  onConfirmBtnTap: () {
                                    Navigator.pop(context);
                                  });
                            } else if (status == 422) {
                              CoolAlert.show(
                                  context: context,
                                  type: CoolAlertType.warning,
                                  title: '',
                                  text: "ایمیل یا رمز عبور اشتباه است",
                                  confirmBtnText: '  تایید',
                                  confirmBtnTextStyle: kConfirmButtonTextStyle,
                                  onConfirmBtnTap: () {
                                    Navigator.pop(context);
                                  });
                            } else if (status == 504 || status == 502) {
                              CoolAlert.show(
                                  context: context,
                                  type: CoolAlertType.error,
                                  title: '!خطا',
                                  text: "خطایی رخ داده است!",
                                  confirmBtnText: '  تایید',
                                  confirmBtnTextStyle: kConfirmButtonTextStyle,
                                  onConfirmBtnTap: () {
                                    Navigator.pop(context);
                                  });
                            }
                          }
                        }
                      });
                    },
                    color: Colors.blueAccent,
                    child: Text(
                      'ثبت نام',
                      style: kLoginRegisterForgotButtonTextStyle,
                    ),
                  ),
                ),
              ],
            ),
          ),
        )),
      ),
    );
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }
}
